import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const App(title: 'Practice 1 | AM2'),
    );
  }
}

class App extends StatefulWidget {
  const App({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<App> createState() => _PageState();
}

class _PageState extends State<App> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text(widget.title) ),
      body: const FirstRoute()
    );
  }
}
// Routes
class FirstRoute extends StatelessWidget {
  const FirstRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding( padding: const EdgeInsets.all(20), child: LoginForm() );
  }
}

class SecondRoute extends StatelessWidget {
  final String email;
  final String password;

  const SecondRoute({
    Key? key, 
    this.email = '', 
    this.password = ''
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Valores ingresados')),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                "Haz ingresado el siguiente email:",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  decoration: TextDecoration.none
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                email,
                style: const TextStyle(
                  fontSize: 24,
                  color: Colors.black,
                  decoration: TextDecoration.none
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                "Haz ingresado el siguiente password:",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                  color: Colors.black,
                  decoration: TextDecoration.none
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Text(
                password,
                style: const TextStyle(
                  fontSize: 24,
                  color: Colors.black,
                  decoration: TextDecoration.none
                ),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                minimumSize: const Size.fromHeight(50)
              ),
              onPressed: (){
                Navigator.pop(context);
              },
              child: const Text(
                'Go Back',
                style: TextStyle(
                  fontSize: 24
                ),
              ),
            )
          ]
        ),
      ),
    );
  }
}

// Custom widgets
class LoginForm extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  LoginForm({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextFieldForm(
          label: 'Email',
          controller: emailController,
        ),
        TextFieldForm(
          label: 'Password',
          isPassword: true,
          controller: passwordController,
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            minimumSize: const Size.fromHeight(50)
          ),
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute(
                email: emailController.text, 
                password: passwordController.text
                )
              ),
            );
          },
          child: const Text(
            'Login',
            style: TextStyle(
              fontSize: 24
            ),
          ),
        )
      ],
    );
  }
}

class TextFieldForm extends StatelessWidget {
  final String label;
  final bool isPassword;
  final controller;

  const TextFieldForm({Key? key, this.label = '', this.isPassword = false, this.controller}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: TextField(
        obscureText: isPassword,
        controller: controller,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: label,
        ),
      ),
    );
  }
}
